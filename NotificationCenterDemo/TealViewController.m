//
//  TealViewController.m
//  NotificationCenterDemo
//
//  Created by James Cash on 25-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "TealViewController.h"

@interface TealViewController ()

@property (nonatomic,weak) IBOutlet UIButton* theButton;

@end

@implementation TealViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Adding a second observer for the same notification -- both first viewcontroller and this one will recieve the events and can handle them seperately
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(buttonUpdateNotification:)
     name:@"textChangingNotification"
     object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonUpdateNotification:(NSNotification*)notification
{
    NSString *newText = notification.userInfo[@"newText"];
    [self.theButton setTitle:newText forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
