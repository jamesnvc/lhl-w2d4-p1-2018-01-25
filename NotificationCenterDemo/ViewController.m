//
//  ViewController.m
//  NotificationCenterDemo
//
//  Created by James Cash on 25-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *theLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    // IN first ViewController
    // Adding observer to recieve data
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(labelUpdateNotification:)
     name:@"textChangingNotification"
     object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)labelUpdateNotification:(NSNotification*)notification
{
    NSString *newText = notification.userInfo[@"newText"];
    self.theLabel.text = newText;

}

@end
