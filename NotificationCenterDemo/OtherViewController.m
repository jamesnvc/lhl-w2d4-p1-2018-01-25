//
//  OtherViewController.m
//  NotificationCenterDemo
//
//  Created by James Cash on 25-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "OtherViewController.h"

@interface OtherViewController () <UITextFieldDelegate>

@end

@implementation OtherViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextViewDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"New text = %@", newText);
    // In Second ViewController
    // Sending notification to pass data (newText) to first VC
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"textChangingNotification"
     object:nil
     userInfo:@{@"newText": newText}];
    return YES;
}

@end
